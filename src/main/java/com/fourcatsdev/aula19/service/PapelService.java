package com.fourcatsdev.aula19.service;

import java.util.List;

import com.fourcatsdev.aula19.modelo.Papel;

public interface PapelService {
	public Papel buscarPapelPorId(Long id);
	public Papel buscarPapel(String papel);
	public List<Papel> listarPapel();
}
